﻿#pragma strict
import System.Xml;
var fileText:TextAsset;
static var XMLContent4All:String;

function Start () {
	var parser : XMLReader = XMLReader();
	var XMLContent: String = fileText.text;
	XMLContent4All = XMLContent;
	var node : XMLNode = parser.read(XMLContent4All);
	//LoadAllXML(node, 0)
	LoadAllXML(node, 0);	
}

function Update () {

}
/*
function LoadLocalXML(){
	var parser=new XMLParser();
	var XMLContent: String = fileText.text;
	var node:XMLNode=parser.Parse(XMLContent);
	Debug.Log("Lettura XML generale: numero nodi");
	var thing = "<point-of_interests>";
	//ASC_1info.titleArticle = node.GetValue("iFoodGame>0>point-of_interests>0>poi>0>@name");
	//ASC_1info.TestoArticolo = node.GetValue("iFoodGame>0>point-of_interests>0>poi>0>long_description>0>_text");
	//ASC_1info.dateText = node.GetValue("iFoodGame>0>point-of_interests>0>poi>0>link>0>_text");
	//var node : XMLNode = parser.read(xmlString);
}*/

public function loadInvestigate(node : XMLNode, indent : int)  {
	indent++;
    for (var n : XMLNode in node.children) {
    	//var nodone : XMLNode = n;
        //Debug.Log("loadinvestigate: "+n.tagName);
    	if (n.tagName=="suspect") {
    		for (var nn : XMLNode in n.children) {
        		//Debug.Log("loadinvestigate2: "+nn.tagName);
	        	if (nn.tagName=="poi_id") GUIVariables.investSuspectId.Push(parseInt(nn.textValue));
	        	if (nn.tagName=="likelihood") GUIVariables.investSuspectLikelihood.Push(nn.textValue);
	        	if (nn.tagName=="parameters_of_interest") {
					var investSuspectParamId : Array = new Array();
					var investSuspectParamValue : Array = new Array();
					var investSuspectParam : Array = new Array();
	        		for (var nnn : XMLNode in nn.children) {
	        			if (nnn.tagName=="parameter_id") {
	        				//Debug.Log("loadinvestigate3: "+nnn.tagName);
		        			investSuspectParamId.Push(parseInt(nnn.textValue)); 
		        			var stringa : String = nnn.attributes["value"];
		        			investSuspectParamValue.Push(parseFloat(stringa));
		        		}
	        		}
	        		investSuspectParam.Push(investSuspectParamId); 
	        		investSuspectParam.Push(investSuspectParamValue); 
	        		GUIVariables.investSuspectParams.Push(investSuspectParam); 
	        	}
        	}
        }
        if (n.tagName=="suspects") {
       		loadInvestigate(n, indent);
        }
    }
}

public function LoadAllXML(node : XMLNode, indent : int) {
    indent++;
    for (var n : XMLNode in node.children) {
        var attr : String = " ";
        //Debug.Log(n.attributes);
        for(var p : DictionaryEntry in n.attributes){ 
            attr += "[" + p.Key + ": " + p.Value + "] ";
            if (p.Key=="name") var nameGameObject = p.Value;
        }
        //if (n.tagName=="poi" && nameGameObject=="poi name 1") {
        if (n.tagName=="poi") {
        	if (GameObject.Find(nameGameObject)!=null) GameObject.Find(nameGameObject).SendMessage("readPOI", n);
        }
    	//Debug.Log("figli "+n.children.Count);
    	//if (n.children.Count>0) Debug.Log("tipo figli "+n.children[0]);
    
        if (n.tagName=="suspect") {
        	loadInvestigate(node, indent);
        }
	    if (n.children.Count>0){
	    	var nodo : XMLNode = n.children[0];
	    	//Debug.Log("nome tag figli "+nodo.tagName);
	    }
    	//Debug.Log("padre: "+n.parentNode.tagName);
        //Debug.Log(String("-"[0], indent-1) + " " + n.tagName + attr);
        LoadAllXML(n, indent);
    }
}