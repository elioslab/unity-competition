﻿#pragma strict
import System.Collections.Generic;
import System.Linq;
	var startLanguage: String = "eng";
	var textENGLangFile: TextAsset;
	var textFRALangFile: TextAsset;
	var textITALangFile: TextAsset;
	var textESPLangFile: TextAsset;
	var textPORLangFile: TextAsset;
	var textGERLangFile: TextAsset;
	
	var pGeneralSkin : GUISkin;
	var pcTextBoxStyle : GUIStyle;
	var androidTextBoxStyle : GUIStyle;
	var dataPath : String = "data/";
	var imagePath : String = "images/";
	var loadImgFromInternet : boolean;
	var buttonParameterIcon : Texture2D;
	var buttonViewPoiDetailsIcon : Texture2D;
	var circleMaterial: Material;
	static var generalSkin : GUISkin;
	static var TextBoxStyle : GUIStyle;
	static var immagine : Texture2D;
	static var publicDataPath : String;
	static var publicimagePath : String;
	static var ploadImgFromInternet : boolean;
	static var pButtonParameterIcon : Texture2D;
	static var pButtonViewPoiDetailsIcon : Texture2D;
	static var ptextLangFile: TextAsset;
    static var Dizionario : Hashtable = new Hashtable();
    static var pCircleMaterial: Material;
    static var suspect_dictionary : Hashtable = new Hashtable();
	static var SuspectedPOIs = new Array();
	static var winningGame: boolean = false;
	static var solutionPoiId: int=-1;
	static var solutionParameterId: int=-1;
	
	static var musicClip: String;
	
	static var NewspaperName: String;
	static var dateText: String;
	static var titleArticle_start: String;
	static var TestoArticolo_start: String;
	static var titleArticle_good: String;
	static var TestoArticolo_good: String;
	static var titleArticle_wrong: String;
	static var TestoArticolo_wrong: String;
	
	static var investSuspectId : Array = new Array();
	static var investSuspectLikelihood : Array = new Array();
	static var investSuspectParams : Array = new Array();
	static var gioca : boolean = false;
	static var playerType : String;
	static var fromLevel : String;
	static var activeLevel : String;
	public var dressed : boolean=false;
	static var staticDressed : boolean=false;
	

public class suspectedPOI {
	var poiId : int;
	var poiName: String;
	var parametersId : Array;
	var parametersName : Array;
	var parametersValue : Array;
	var parametersSuspect : Array;
	
	function suspectedPOI() {
    	//init code when instance of class created
		parametersId = new Array();
		parametersName = new Array();
		parametersValue = new Array();
		parametersSuspect = new Array();
    }
	
	function addParam(parameter_id:int, parameter_name:String, parameter_Value:float) {
		//devo controllare che non ci sia due volte lo stesso id ecc
		var exist: boolean = false;
		for (var i:int=0; i<this.parametersId.length; ++i) {
			if (this.parametersId[i]==parameter_id) exist=true; // il paramtero è già nei sospetti
		}
		if (!exist) { //il parametro non esiste e quindi lo aggiungo
			this.parametersId.Add(parameter_id);
			this.parametersName.Add(parameter_name);
			this.parametersValue.Add(parameter_Value);
			this.parametersSuspect.Add(false);
			//siccome ho compiuto una modifica avviso:
			trysolution.loadDataSuspected=true;
		} 
	}
	
	function deleteParameters(parameter_id:int){
		for (var i:int=0; i<this.parametersId.length; ++i) { //controllo se esiste
			if (this.parametersId[i]==parameter_id) { //esiste!!
				this.parametersId.RemoveAt(i); //quindi lo rimuovo
				this.parametersName.RemoveAt(i); //quindi lo rimuovo
				this.parametersValue.RemoveAt(i); //quindi lo rimuovo
				this.parametersSuspect.RemoveAt(i); //quindi lo rimuovo
				//siccome ho compiuto una modifica avviso:
				trysolution.loadDataSuspected=true;
			} 
		}
	}
}

function Start () {
	//immagine =  Image;
	generalSkin = pGeneralSkin;
	publicDataPath = dataPath;
	publicimagePath = imagePath;
	pButtonParameterIcon = buttonParameterIcon;
	pButtonViewPoiDetailsIcon = buttonViewPoiDetailsIcon;
	changeLang_dictionary(startLanguage);
	pCircleMaterial = circleMaterial;
	
	//#if UNITY_EDITOR
    //Debug.Log("Unity Editor");
    //TextBoxStyle = pcTextBoxStyle;
	//#endif

	#if UNITY_ANDROID
    TextBoxStyle = androidTextBoxStyle;
	#endif
	
	#if UNITY_IPHONE
    TextBoxStyle = androidTextBoxStyle;
	#endif

	#if UNITY_STANDALONE_OSX
    TextBoxStyle = pcTextBoxStyle;
	#endif

	#if UNITY_STANDALONE_WIN
    TextBoxStyle = pcTextBoxStyle;
	#endif	
}

function update() {
	staticDressed = dressed;
}

function changeLang_dictionary(lang: String) { //creo l'array multidimensionale
	Dizionario.Clear();
	if (lang == "eng") ptextLangFile = textENGLangFile;
	if (lang == "ita" && !textITALangFile) ptextLangFile = textITALangFile;
	if (lang == "por" && !textPORLangFile) ptextLangFile = textPORLangFile;
	if (lang == "fra" && !textFRALangFile) ptextLangFile = textFRALangFile;
	if (lang == "ger" && !textGERLangFile) ptextLangFile = textGERLangFile;
	if (lang == "esp" && !textGERLangFile) ptextLangFile = textESPLangFile;
	if (!ptextLangFile) ptextLangFile = textENGLangFile; // se non esiste scegli comunque inglese
	create_dictionary();
}

function create_dictionary() { //creo l'array multidimensionale
	if (!ptextLangFile) return;
	var Readline = ptextLangFile.text.Split('\n'[0]);
	for ( var i = 0; i < Readline.length; i ++ ) {
		
		if(Readline[i]!="") { //se ci fosse una riga vuota non la considero
			var key: String = Readline[i].Split('|'[0])[0];	
			var word: String = Readline[i].Split('|'[0])[1];
			key = key.Substring(1, key.Length - 2);
			word = word.Substring(1, word.Length - 2);
			Dizionario.Add(key, word);
		}
	}
}

static function langText (word : String): String {	
	if (Dizionario.Contains(word)) {
		return Dizionario[word];
	} else {
		return word;
	}
}
/*function OnGUI() { 
	GUI.Label(Rect(100,100,100,100), playerType);
}*/