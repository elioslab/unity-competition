﻿import System.Collections.Generic;
#pragma strict
var viewBookSuspect : boolean=false;
var imgButton: Texture2D;
private var suspectPOI : suspectedPOI; 
private var dimensione:float = 246.0f;
private var Ydimensione:float =256.0f;
private var k : int = 0;
private var trysolution: boolean = false;
private var sure : boolean = false;
//private var selGridInt : int = 1000;
//private var selStrings : String[] = ["pippo","mimmo","suolo"];
static var parameterSuspectedStrings : List.<String> = new List.<String>();
static var parameterSuspectedgroupsLength : Array = new Array();
static var parameterSuspectedgroupsName : Array = new Array();
static var parameterSuspectedInt : int = 10000;
static var loadDataSuspected=true; //variabile utilizzata per capire se sono stati aggiunti o tolti dei sospetti per avviare la funzione di lettura sospetti

private var tmpInt : int = parameterSuspectedInt;
private var scrollPosition : Vector2;


function Start () {

}
function Update () {

	if (trysolution) {
		var TMPPOI: int;	
		var TMPPARAMETER : int;		
		for (var ii=0; ii<GUIVariables.SuspectedPOIs.length; ++ii) {
			suspectPOI = GUIVariables.SuspectedPOIs[ii];
			for (var jj=0; jj<suspectPOI.parametersName.length; jj++) {
				if (suspectPOI.parametersSuspect[jj] == true) {
					//suspectPOI.parametersId[j];
					if (suspectPOI.poiId==GUIVariables.solutionPoiId && suspectPOI.parametersId[jj]==GUIVariables.solutionParameterId) {
						GUIVariables.winningGame=true;						
					} else {
						GUIVariables.winningGame=false;							
					}
					TMPPOI = suspectPOI.poiId;	
					TMPPARAMETER = suspectPOI.parametersId[jj];							
				}
			}
		}
		Application.LoadLevel ("finish");
		Debug.Log(GUIVariables.winningGame + " . Hai selezionato: il poi "+TMPPOI+" ed il parametro: "+TMPPARAMETER);
		Debug.Log("Risposta corretta: il poi "+GUIVariables.solutionPoiId+" ed il parametro: "+GUIVariables.solutionParameterId);
		}
}

function Read_suspected() : int {
		var pick : boolean = false;
		//parameterSuspectedInt = 1000;
		k=0;
		var SuspectedInt : int=10000;
		parameterSuspectedgroupsLength.Clear();
		parameterSuspectedgroupsName.Clear();
		parameterSuspectedStrings.Clear();
		for (var i=0; i<GUIVariables.SuspectedPOIs.length; ++i) {
			suspectPOI = GUIVariables.SuspectedPOIs[i];
			var nRadioGroup : int = 0;
			for (var j=0; j<suspectPOI.parametersName.length; j++) {
				pick = suspectPOI.parametersSuspect[j];	
				if (pick==true) SuspectedInt=k;
				nRadioGroup++;
				parameterSuspectedStrings.Add(suspectPOI.parametersName[j]);
			}
			parameterSuspectedgroupsLength.Add(nRadioGroup);
			parameterSuspectedgroupsName.Add(suspectPOI.poiName);
			k++;
		}
		loadDataSuspected=false;
		return SuspectedInt;
}
function selectSolution(selVal: int) {
		//assegno il valore al POI sospettato.
		k = 0;
		var pick:boolean;
		for (var i=0; i<GUIVariables.SuspectedPOIs.length; ++i) {
			suspectPOI = GUIVariables.SuspectedPOIs[i];
			for (var j=0; j<suspectPOI.parametersName.length; j++) {
				if (selVal==k) {
					suspectPOI.parametersSuspect[j]=true;
					} else {
					suspectPOI.parametersSuspect[j]=false;
					}
				k++;
			}
		}		
}

function OnGUI () {
	
	GUI.skin = GUIVariables.generalSkin;
	if (GUI.Button(Rect(Screen.width-110,0,107,77), GUIContent("", imgButton, "sospetti"), "BoxInvisible")) viewBookSuspect=!viewBookSuspect;
	if (viewBookSuspect)  {
		GUI.skin = GUIVariables.generalSkin;
		
		var POIsospettatiTXT: String ="";	
		
		//if (parameterSuspectedStrings.Count>5) Ydimensione = 55+parameterSuspectedStrings.Count*36;

		Ydimensione = 250;
		GUI.Box(Rect(Screen.width-(dimensione+10),60,dimensione,Ydimensione), GUIVariables.langText("giveYourSolution"), "pergamena");
		if (loadDataSuspected==true) {
			parameterSuspectedInt = Read_suspected();
		}
		var countAllRadio : int =0;
		
		var opScroll: GUILayoutOption[] = new GUILayoutOption[2];
		opScroll[0] = GUILayout.Height(Ydimensione-50);
		opScroll[1] = GUILayout.Width(dimensione+20);
		GUILayout.BeginArea(Rect(Screen.width-(dimensione+10),90,dimensione,Ydimensione-50)); 		 
		scrollPosition = GUILayout.BeginScrollView (scrollPosition, false, false, opScroll); //This stores the position of the scroll bar to wherever you have moved it
		var opBox: GUILayoutOption[] = new GUILayoutOption[2];
		for (var i=0; i<parameterSuspectedgroupsLength.Count; ++i) {
			var nRadioCount: int = parameterSuspectedgroupsLength[i];
			var poiName : String = parameterSuspectedgroupsName[i];
			if (poiName.Length>25 && nRadioCount==1) poiName = poiName.Substring(0,10)+" ... "+poiName.Substring(poiName.Length-14,14);
			countAllRadio += nRadioCount;
			opBox[0] = GUILayout.Height((nRadioCount*35)-5);
			opBox[1] = GUILayout.Width(240);
			GUILayout.Box(poiName,"radioGroup",opBox);
		}
		GUILayout.Label("","invisibleLabel");//serve per dare spazio sotto lo scroll
		GUILayout.EndScrollView ();
		GUILayout.EndArea();
		
		
		///////////////////////////////////////////////
		GUILayout.BeginArea(Rect(Screen.width-(dimensione),90,dimensione,Ydimensione-50));
		opScroll[1] = GUILayout.Width(dimensione);
		scrollPosition = GUILayout.BeginScrollView (scrollPosition, false, true, opScroll); //This stores the position of the scroll bar to wherever you have moved it
		    
		var opSelect: GUILayoutOption[] = new GUILayoutOption[2];
		opSelect[0] = GUILayout.Width(210);
		opSelect[1] = GUILayout.Height(100);
		parameterSuspectedInt = GUILayout.SelectionGrid(parameterSuspectedInt, parameterSuspectedStrings.ToArray(),1, "radio");
		GUILayout.Label("","invisibleLabel");//serve per dare spazio sotto lo scroll
		GUILayout.EndScrollView ();
		GUILayout.EndArea ();
		
		if (tmpInt!=parameterSuspectedInt) selectSolution(parameterSuspectedInt);
		tmpInt = parameterSuspectedInt;
		
		//nRadioGroup = parameterSuspectedgroupsLength[0]; 
		if (GUI.Button(Rect(Screen.width-(dimensione+5), Ydimensione+70, dimensione, 60), GUIVariables.langText("giveSolution"), "styledButton")) {
			sure=true;	
		}
	}
	if (sure==true) {
		GUI.Box(Rect(Screen.width/2-(dimensione/2),Screen.height/2,dimensione,100), GUIVariables.langText("areUSure")+"?");
		if (GUI.Button(Rect(Screen.width/2-(35)-40, Screen.height/2+30, 70, 60), GUIVariables.langText("Yes"), "styledButton")) {
			trysolution = true;
			sure=!sure;
		} 
		if (GUI.Button(Rect(Screen.width/2-(35)+40, Screen.height/2+30, 70, 60), GUIVariables.langText("No"), "styledButton")) {
			viewBookSuspect=!viewBookSuspect;
			sure=!sure;
			trysolution = false;
		}
	}
}