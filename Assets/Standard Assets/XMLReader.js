﻿class XMLReader {
    private var TAG_START : char = "<"[0];
    private var TAG_END     : char = ">"[0];
    private var SPACE : char = " "[0];
    private var QUOTE : char = "\""[0];
    private var SLASH : char = "/"[0];
    private var EQUALS : char = "="[0];
    private var BEGIN_QUOTE : String = "" + EQUALS + QUOTE;    

    function read(xml : String) : XMLNode {
        var index : int = 0;
        var lastIndex : int = 0;
        var rootNode : XMLNode = XMLNode();
        var currentNode : XMLNode = rootNode;        

        while (true) {
            index = xml.IndexOf(TAG_START, lastIndex);
            if (index < 0 || index >= xml.Length)   break;
            index++; // skip the tag-char                

            lastIndex = xml.IndexOf(TAG_END, index);
            if (lastIndex < 0 || lastIndex >= xml.Length) break;            

            var tagLength = lastIndex - index;
            var xmlTag : String = xml.Substring(index, tagLength);            

            // The tag starts with "</", it is thus an end tag
            if (xmlTag[0] == SLASH) {
                currentNode = currentNode.parentNode;
                continue;
            }

            var openTag = true;
            // The tag ends in "/>", it is thus a closed tag
            if (xmlTag[tagLength - 1] == SLASH) {
                xmlTag = xmlTag.Substring(0, tagLength - 1); // cut away the slash
                openTag = false;
            }
            
            //s += readTag(xmlTag, indent);
            var node : XMLNode = parseTag(xmlTag);
            node.parentNode = currentNode;
            currentNode.children.Push(node);            

            if (openTag) {
                var nextNode : int;
                nextNode = xml.IndexOf(TAG_START, lastIndex);
                if (nextNode >= 0 && nextNode < xml.Length) {
                    node.textValue = xml.Substring(lastIndex+1, nextNode-lastIndex-1).Trim();
                }
                else {
                    node.textValue = "";
                }
                currentNode = node;
            }
        };      
        return rootNode;
    }

    function parseTag(xmlTag : String) : XMLNode {
        var node : XMLNode = new XMLNode();    

        var nameEnd : int = xmlTag.IndexOf(SPACE, 0);
        if (nameEnd < 0) {
            node.tagName = xmlTag;
            return node;
        }

        var tagName : String = xmlTag.Substring(0, nameEnd);
        node.tagName = tagName;        
        var attrString : String = xmlTag.Substring(nameEnd, xmlTag.Length - nameEnd);
        return parseAttributes(attrString, node);
    }

    

    function parseAttributes(xmlTag : String, node : XMLNode) : XMLNode {
        var index : int = 0;
        var attrNameIndex : int = 0;
        var lastIndex : int = 0;        

        while (true) {
            index = xmlTag.IndexOf(BEGIN_QUOTE, lastIndex);
            if (index < 0 || index > xmlTag.Length) break;            

            attrNameIndex = xmlTag.LastIndexOf(SPACE, index);
            if (attrNameIndex < 0 || attrNameIndex > xmlTag.Length) break;
            attrNameIndex++; // skip space char
            var attrName : String = xmlTag.Substring(attrNameIndex, index - attrNameIndex);            

            index += 2; // skip the equal and quote chars                

            lastIndex = xmlTag.IndexOf(QUOTE, index);
            if (lastIndex < 0 || lastIndex > xmlTag.Length) break;            

            var tagLength : int = lastIndex - index;
            var attrValue : String = xmlTag.Substring(index, tagLength);
            node.attributes[attrName] = attrValue;
        };        

        return node;
    }
}