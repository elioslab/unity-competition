﻿using UnityEngine;
using System.Collections;

public class CharacterCollision : MonoBehaviour
{
	private int score = 0;
	
	public GUISkin gSkin;
	
	void OnGUI()
	{
		GUI.skin = gSkin;
		
		GUILayout.BeginArea(new Rect(10, 10, 100,100));
		GUILayout.Label(score.ToString());
		GUILayout.EndArea();
	}
	
	// Use this for initialization
	void Start ()
	{

	}
	
	// Update is called once per frame
	void Update ()
	{
		//GameObject[] list = GameObject.FindGameObjectsWithTag("red");
		//Debug.Log(list.Length);
	}
	
	void OnControllerColliderHit(ControllerColliderHit hit)
	{
		if (hit.gameObject.tag == "green")
		{
			Destroy(hit.gameObject);
			score++;
		}
		if (hit.gameObject.tag == "red")
		{
			Destroy(hit.gameObject);
			score--;
		}
	}
}