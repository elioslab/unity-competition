using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public interface GridData
{
	int Width { get; }
	int Height { get; }
	float GetValue(int x, int y);
}

public class GridDisplay : MonoBehaviour
{
	MeshRenderer meshRenderer;
	MeshFilter meshFilter;
	Mesh mesh;

	GridData data;

	[SerializeField]
	Material material;
	
	[SerializeField]
	Color neutralColor = Color.white;
	
	[SerializeField]
	Color positiveColor = Color.green;
	
	[SerializeField]
	Color positive2Color = Color.green;

	[SerializeField]
	Color positive3Color = Color.green;
	
	[SerializeField]
	Color negativeColor = Color.red;
	
	[SerializeField]
	Color negative2Color = Color.red;

	[SerializeField]
	Color negative3Color = Color.red;
	
	Color[] colors;

	public void SetGridData(GridData m) { data = m; }

	public void CreateMesh(Vector3 bottomLeftPos, float gridSize)
	{
		mesh = new Mesh();
		mesh.name = name;
		meshFilter = gameObject.AddComponent(typeof(MeshFilter)) as MeshFilter;
		meshRenderer = gameObject.AddComponent(typeof(MeshRenderer)) as MeshRenderer;
		
		meshFilter.mesh = mesh;
		meshRenderer.material = material;
		
		float objectHeight = transform.position.y;
		float staX = 0;
		float staZ = 0;
		
		List<Vector3> verts = new List<Vector3>();
		for (int y = 0; y < data.Height; ++y)
		{
			for (int x = 0; x < data.Width; ++x)
			{
				Vector3 bl = new Vector3(staX + ( x    * gridSize), objectHeight, staZ + ( y    * gridSize));
				Vector3 br = new Vector3(staX + ((x+1) * gridSize), objectHeight, staZ + ( y    * gridSize));
				Vector3 tl = new Vector3(staX + ( x    * gridSize), objectHeight, staZ + ((y+1) * gridSize));
				Vector3 tr = new Vector3(staX + ((x+1) * gridSize), objectHeight, staZ + ((y+1) * gridSize));

				verts.Add(bl);
				verts.Add(br);
				verts.Add(tl);
				verts.Add(tr);
			}
		}

		List<Color> colorslist = new List<Color>();
		for (int y = 0; y < data.Height; ++y)
		{
			for (int x = 0; x < data.Width; ++x)
			{
				colorslist.Add(Color.white);
				colorslist.Add(Color.white);
				colorslist.Add(Color.white);
				colorslist.Add(Color.white);
			}
		}
		colors = colorslist.ToArray();
		
		List<Vector3> norms = new List<Vector3>();
		for (int y = 0; y < data.Height; ++y)
		{
			for (int x = 0; x < data.Width; ++x)
			{
				norms.Add(Vector3.up);
				norms.Add(Vector3.up);
				norms.Add(Vector3.up);
				norms.Add(Vector3.up);
			}
		}
		
		List<Vector2> uvs = new List<Vector2>();
		for (int y = 0; y < data.Height; ++y)
		{
			for (int x = 0; x < data.Width; ++x)
			{
				uvs.Add(new Vector2(0, 0));
				uvs.Add(new Vector2(1, 0));
				uvs.Add(new Vector2(0, 1));
				uvs.Add(new Vector2(1, 1));
			}
		}
		
		List<int> tris = new List<int>();
		for (int i = 0; i < verts.Count; i+=4)
		{
			int bl = i;
			int br = i+1;
			int tl = i+2;
			int tr = i+3;

			tris.Add(bl);
			tris.Add(tl);
			tris.Add(br);
			
			tris.Add(tl);
			tris.Add(tr);
			tris.Add(br);
		}

		mesh.vertices = verts.ToArray();
		mesh.normals = norms.ToArray();
		mesh.uv = uvs.ToArray();
		mesh.colors = colors;
		mesh.triangles = tris.ToArray();
	}
	
	void SetColor(int x, int y, Color c)
	{
		int i = ((y * data.Width) + x) * 4;
		colors[i] = c;
		colors[i+1] = c;
		colors[i+2] = c;
		colors[i+3] = c;
	}

	void Update()
	{
		for (int y = 0; y < data.Height; ++y)
		{
			for (int x = 0; x < data.Width; ++x)
			{
				float value = data.GetValue(x, y);
				Color c = neutralColor;
				if (value > 0.75f)      	{ c = Color.Lerp(positive2Color, positive3Color, (value-0.5f)/0.5f); }
				else if (value > 0.5f)  	{ c = Color.Lerp(positiveColor, positive2Color, (value-0.5f)/0.5f); }
				else if (value > 0)     	{ c = Color.Lerp(neutralColor,  positiveColor,  value/0.5f); }
				else if (value < -0.5f)		{ c = Color.Lerp(negativeColor, negative2Color, -(value+0.5f)/0.5f); }
				else if (value < -0.75f)	{ c = Color.Lerp(negative2Color, negative3Color, -(value+0.5f)/0.5f); }
				else                    	{ c = Color.Lerp(neutralColor,  negativeColor,  -value/0.5f); }

				SetColor(x, y, c);
			}
		}
		mesh.colors = colors;
	}
}
