﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;

public class SphereCollision : MonoBehaviour
{
	void Start (){	}
	
	void Update (){	}

	void OnCollisionEnter(Collision hit)
	{
		if (hit.gameObject.tag == "AIPlayer") 
		{
			Default agent = hit.gameObject.GetComponent<Default>();

			if(this.gameObject.tag == "green")
			{
				agent.score++;
				//Debug.Log ("Bravo " + agent.name);
			}
			else if(this.gameObject.tag == "red")
			{
				agent.score--;
				//Debug.Log ("Male " + agent.name);
			}
			
			Destroy (this.gameObject);
		}
	}
}