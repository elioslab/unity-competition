using System;
using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (NavMeshAgent))]
    [RequireComponent(typeof (ThirdPersonCharacter))]
	public class PiaDaum : Default
    {
        public NavMeshAgent agent { get; private set; }             // the navmesh agent required for the path finding
        public ThirdPersonCharacter character { get; private set; } // the character we are controlling
        public Transform target;                                    // target to aim for
        public GameObject[] greenSpheres,redSpheres;
        public GameObject greenSphere, nearestSphere, playerObject;
        public Transform[] TgreenSpheres, TredSpheres;
        public Transform nearestGreenSphere, nearestRedSphere, TplayerObject;
        float distance;
        public Vector3 sub;


        private void Start()
        {
            // get the components on the object we need ( should not be null due to require component so no need to check )
            agent = GetComponentInChildren<NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();

	        agent.updateRotation = false;
	        agent.updatePosition = true;

        


        }


        private void Update()
        {

            SetTarget(target);
            
            TplayerObject  = GameObject.Find("Player").transform;

            Vector3 runPosition = target.position - TplayerObject.position;

            if (target.tag == "red")
                agent.SetDestination(runPosition);

            else if (target.tag == "green")
                agent.SetDestination(target.position);

            if (agent.remainingDistance > agent.stoppingDistance)
                character.Move(agent.desiredVelocity, false, false);
            else
                character.Move(Vector3.zero, false, false);
        }


        public void SetTarget(Transform target)
        {

            greenSpheres = GameObject.FindGameObjectsWithTag("green");

            redSpheres = GameObject.FindGameObjectsWithTag("red");

            TgreenSpheres = Array.ConvertAll<GameObject, Transform>(greenSpheres, x => x.transform);

            TredSpheres = Array.ConvertAll<GameObject, Transform>(redSpheres, x => x.transform);

            nearestRedSphere = GetClosestSphere(TredSpheres);

            nearestGreenSphere = GetClosestSphere(TgreenSpheres);

            playerObject = GameObject.Find("Player");

            //distanza tra il player e la sfera rossa
            float distance = Vector3.Distance(playerObject.transform.position, nearestRedSphere.position);

            if (distance < 2.5)
            {
                this.target = nearestRedSphere;
            }
            else
            {
                this.target = nearestGreenSphere;
            }


        }




        Transform GetClosestSphere(Transform[] greenSpheres)
        {
            Transform tMin = null;
            float minDist = Mathf.Infinity;
            Vector3 currentPos = transform.position;
            foreach (Transform t in greenSpheres)
            {
                float dist = Vector3.Distance(t.transform.position, currentPos);
                if (dist < minDist)
                {

                    tMin = t;
                    minDist = dist;
                }
            }
            return tMin;
        }

    }
}
