﻿using UnityEngine;
using System.Collections;


namespace UnityStandardAssets.Characters.ThirdPerson
{
	[RequireComponent(typeof (NavMeshAgent))]
	[RequireComponent(typeof (ThirdPersonCharacter))]
	public class LaRoccaBollo : Default
	{
		public NavMeshAgent agent { get; private set; } // the navmesh agent required for the path finding
		public ThirdPersonCharacter character { get; private set; } // the character we are controlling
		public Transform target;
		public Vector3 targetPos;
		public GameObject[] sphereList;// target to aim for

		// Use this for initialization
		private void Start()
		{

			agent = GetComponentInChildren<NavMeshAgent>();
			character = GetComponent<ThirdPersonCharacter>();
			sphereList = new GameObject[100];

			agent.updateRotation = true;
			agent.updatePosition = true;
		}


		// Update is called once per frame
		private void Update()
		{
			sphereList = GameObject.FindGameObjectsWithTag ("green");
			targetPos = findCloseBall(sphereList, agent.gameObject.GetComponent<Transform>().position);

			if (targetPos != Vector3.zero) {
				agent.SetDestination (targetPos);
			}
			if (agent.remainingDistance > agent.stoppingDistance) {
				character.Move (agent.desiredVelocity, false, false);
			} 
			else {
				character.Move (Vector3.zero, false, false);
			}

		}

		//method used to find the closest sphere

		public Vector3 findCloseBall (GameObject[] myList, Vector3 position){

			Vector3 realTarget = Vector3.zero;

			if (myList.Length == 0) {
				Debug.Log ("No balls");
			} else {

				foreach (GameObject sphere in myList) {
					if (sphere.GetComponent<Transform> ().position != null) {
						realTarget = sphere.GetComponent<Transform> ().position;
					}
				}
				foreach (GameObject sphere in myList) {
					if (Vector3.Distance(sphere.GetComponent<Transform>().position, 
					                     agent.transform.position) < Vector3.Distance(realTarget,
					                                                 agent.transform.position)){
						realTarget = sphere.GetComponent<Transform>().position;
					}				
				}
			}
			return realTarget;
		}
		
		
		public void SetTarget(Transform target)
		{
			this.target = target;
		}
	}
}

/*public class LaRoccaBalls : MonoBehaviour {

	public float speed = 2f; //speed of ai
	public Transform target;

	private NavMeshAgent nav;
	GameObject sphere;

	void startAI (){
	
		nav = GetComponent<NavMeshAgent> ();
		sphere = GameObject.Find ("sphere");
		target = GameObject.FindGameObjectWithTag ("magenta").transform;

	}
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		moving ();
	}

	void moving(){

		nav.destination = target.position;
		nav.speed = speed;


	}

}
/*
 * public class EnemyAI : MonoBehaviour
{
    public float patrolSpeed = 2f;                          // The nav mesh agent's speed when patrolling.
    public float chaseSpeed = 5f;                           // The nav mesh agent's speed when chasing.
    public float chaseWaitTime = 5f;                        // The amount of time to wait when the last sighting is reached.
    public float patrolWaitTime = 1f;                       // The amount of time to wait when the patrol way point is reached.
    public Transform[] patrolWayPoints;                     // An array of transforms for the patrol route.
    
    
    private EnemySight enemySight;                          // Reference to the EnemySight script.
    private NavMeshAgent nav;                               // Reference to the nav mesh agent.
    private Transform player;                               // Reference to the player's transform.
    private PlayerHealth playerHealth;                      // Reference to the PlayerHealth script.
    private LastPlayerSighting lastPlayerSighting;          // Reference to the last global sighting of the player.
    private float chaseTimer;                               // A timer for the chaseWaitTime.
    private float patrolTimer;                              // A timer for the patrolWaitTime.
    private int wayPointIndex;                              // A counter for the way point array.
    
    
    void Awake ()
    {
        // Setting up the references.
        enemySight = GetComponent<EnemySight>();
        nav = GetComponent<NavMeshAgent>();
        player = GameObject.FindGameObjectWithTag(Tags.player).transform;
        playerHealth = player.GetComponent<PlayerHealth>();
        lastPlayerSighting = GameObject.FindGameObjectWithTag(Tags.gameController).GetComponent<LastPlayerSighting>();
    }
    
    
    void Update ()
    {
        // If the player is in sight and is alive...
        if(enemySight.playerInSight && playerHealth.health > 0f)
            // ... shoot.
            Shooting();
        
        // If the player has been sighted and isn't dead...
        else if(enemySight.personalLastSighting != lastPlayerSighting.resetPosition && playerHealth.health > 0f)
            // ... chase.
            Chasing();
        
        // Otherwise...
        else
            // ... patrol.
            Patrolling();
    }
    
    
    void Shooting ()
    {
        // Stop the enemy where it is.
        nav.Stop();
    }
    
    
    void Chasing ()
    {
        // Create a vector from the enemy to the last sighting of the player.
        Vector3 sightingDeltaPos = enemySight.personalLastSighting - transform.position;
        
        // If the the last personal sighting of the player is not close...
        if(sightingDeltaPos.sqrMagnitude > 4f)
            // ... set the destination for the NavMeshAgent to the last personal sighting of the player.
            nav.destination = enemySight.personalLastSighting;
        
        // Set the appropriate speed for the NavMeshAgent.
        nav.speed = chaseSpeed;
        
        // If near the last personal sighting...
        if(nav.remainingDistance < nav.stoppingDistance)
        {
            // ... increment the timer.
            chaseTimer += Time.deltaTime;
            
            // If the timer exceeds the wait time...
            if(chaseTimer >= chaseWaitTime)
            {
                // ... reset last global sighting, the last personal sighting and the timer.
                lastPlayerSighting.position = lastPlayerSighting.resetPosition;
                enemySight.personalLastSighting = lastPlayerSighting.resetPosition;
                chaseTimer = 0f;
            }
        }
        else
            // If not near the last sighting personal sighting of the player, reset the timer.
            chaseTimer = 0f;
    }

    
    void Patrolling ()
    {
        // Set an appropriate speed for the NavMeshAgent.
        nav.speed = patrolSpeed;
        
        // If near the next waypoint or there is no destination...
        if(nav.destination == lastPlayerSighting.resetPosition || nav.remainingDistance < nav.stoppingDistance)
        {
            // ... increment the timer.
            patrolTimer += Time.deltaTime;
            
            // If the timer exceeds the wait time...
            if(patrolTimer >= patrolWaitTime)
            {
                // ... increment the wayPointIndex.
                if(wayPointIndex == patrolWayPoints.Length - 1)
                    wayPointIndex = 0;
                else
                    wayPointIndex++;
                
                // Reset the timer.
                patrolTimer = 0;
            }
        }
        else
            // If not near a destination, reset the timer.
            patrolTimer = 0;
        
        // Set the destination to the patrolWayPoint.
        nav.destination = patrolWayPoints[wayPointIndex].position;
    }
}*/