﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;

namespace UnityStandardAssets.Characters.ThirdPerson{
	
	[RequireComponent(typeof (NavMeshAgent))]
	[RequireComponent(typeof (ThirdPersonCharacter))]
	
	public class AguirreViviani : Default {

		public NavMeshAgent agent { get; private set; }             // the navmesh agent required for the path finding
		public ThirdPersonCharacter character { get; private set; }
		public Transform target;// the character we are controlling
		public Vector3 dest;
		
		// Use this for initialization
		void Start () {
			
			// get the components on the object we need ( should not be null due to require component so no need to check )
			agent = GetComponentInChildren<NavMeshAgent>();
			character = GetComponent<ThirdPersonCharacter>();
			
			agent.updateRotation = false;
			agent.updatePosition = true;
			
			//dest = Vector3.zero;
			
			//GameObject palla = GameObject.FindGameObjectWithTag ("yellow");
			//dest = palla.GetComponent<Transform> ().position;
		}
		
		
		// Update is called once per frame
		void Update () {
			//Vector3 p = new Vector3(1,0,1);
			//GameObject palla = GameObject.FindGameObjectWithTag ("green");
			GameObject[] palle = GameObject.FindGameObjectsWithTag ("green");
			
			//dest = palla.GetComponent<Transform> ().position; 
			dest = findDest(palle);
			
			if (dest != null)
				agent.SetDestination(dest);
			
			if (agent.remainingDistance > agent.stoppingDistance)
				character.Move(agent.desiredVelocity, false, false);
			
			else
				character.Move(Vector3.zero, false, false);
			
		}
		
		public Vector3 findDest(GameObject[] ArraydiPalle)
		{
			Vector3 temp = Vector3.zero; 
			Vector3 pos = agent.gameObject.GetComponent<Transform> ().position;
			float dist = 10000;
			
			
			//yield return new WaitForSeconds (1);
			foreach (GameObject palla in ArraydiPalle) {
				
				if(palla.gameObject.tag == "green"){
					
					temp = palla.GetComponent<Transform> ().position; 
					if (temp != null) {
						
						if (Vector3.Distance (pos, temp) < dist)
							dist = Vector3.Distance (pos, temp);
					}
					
				}
				
			}
			
			foreach (GameObject palla in ArraydiPalle) {
				
				if (Vector3.Distance (pos, palla.GetComponent<Transform> ().position) == dist)
					temp = palla.GetComponent<Transform> ().position;				
			}
			
			return temp;
		}
		
		/*GameObject Vicino (GameObject[] ArraydiPalle, float dist, Vector3 pos)
		{
			GameObject r;
			foreach (GameObject palla in ArraydiPalle) {
				
				if (Vector3.Distance (pos, palla.GetComponent<Transform> ().position) == dist)
					return palla;
				else
					return r;
			}
		}*/
		
		public void SetTarget(Transform target)
		{
			this.target = target;
		}
		
	}
}
