using System;
using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(ThirdPersonCharacter))]
	public class AssantiBeccaria : Default
    {
        public NavMeshAgent agent { get; private set; }             // the navmesh agent required for the path finding
        public ThirdPersonCharacter character { get; private set; } // the character we are controlling
        public Transform target;                                    // target to aim for

        public GameObject[] objgreen;
        public GameObject[] objred;

        private float minGreenDistance = -1;
        private bool escape; // varible to know if the character needs to go away from the spheres spawning point
        private Vector2 spawningPoint;

        private void Start()
        {
            // get the components on the object we need ( should not be null due to require component so no need to check )
            agent = GetComponentInChildren<NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();

            agent.autoRepath = true;
            escape = false;

            agent.updateRotation = false;
            agent.updatePosition = true;
        }


        private void Update()
        {
            objgreen = GameObject.FindGameObjectsWithTag("green");
            objred = GameObject.FindGameObjectsWithTag("red");

            ChaseGreenBall();
            AvoidRedBall();

            if(escape)
            EscapeFromSpawningPoint();


            if (agent.remainingDistance > agent.stoppingDistance)
            {
                character.Move(agent.desiredVelocity, false, false);
            }
            else
            {
                character.Move(Vector3.zero, false, false);
                if (escape)
                    escape = false;
            }
        }


        public void SetTarget(Transform target)
        {
            this.target = target;
        }

        public void ChaseGreenBall()
        {
            if (objgreen.Length != 0 /*&& escape == false*/)
            {
                if(spawningPoint == null)
                {
                    spawningPoint = new Vector2(objgreen[0].transform.position.x, objgreen[0].transform.position.z);
                }

                foreach (GameObject o in objgreen)
                {
                    Transform oldTarget;

                    if (minGreenDistance < 0)
                    {
                        target = o.transform;
                        minGreenDistance = agent.remainingDistance;

                        if (target != null)
                        {
                            agent.SetDestination(target.position);
                        }
                        else
                        {
                            minGreenDistance = -1;
                           // escape = true;
                        }
                    }
                    else
                    {
                        oldTarget = target;
                        target = o.transform;

                        if (agent.remainingDistance >= minGreenDistance)
                        {
                            target = oldTarget;

                            if (target != null)
                            {
                                agent.SetDestination(target.position);
                            }
                            else
                            {
                                minGreenDistance = -1;
                                //escape = true;
                            }
                        }
                        else
                        {
                            minGreenDistance = agent.remainingDistance;

                            if (target != null)
                            {
                                agent.SetDestination(target.position);
                            }
                            else
                            {
                                minGreenDistance = -1;
                                //escape = true;
                            }
                        }
                    }
                }

            }
            else { target = agent.transform; }

            //if (agent.remainingDistance > agent.stoppingDistance)
            //    character.Move(agent.desiredVelocity, false, false);
            //else
            //    character.Move(Vector3.zero, false, false);
        }

        public void AvoidRedBall()
        {
            Vector3 backMovement = new Vector3(0, 0, -3);

            if (objred.Length != 0)
            {
                if (spawningPoint == null)
                {
                    spawningPoint = new Vector2(objred[0].transform.position.x, objred[0].transform.position.z);
                }

                foreach (GameObject o in objred)
                {
                    if ((agent.transform.position - o.transform.position).magnitude < 2)
                    {
                        if (agent.nextPosition != Vector3.zero)
                        {
                            character.Move(-(agent.nextPosition - agent.transform.position), false, false);
                           // Debug.Log("deviazione in movimento");
                        }
                        else
                        {
                            character.Move(backMovement , false, false);
                               // Debug.Log("deviazione da fermo");
                        }
                    }
                }
            }
        }

        public void EscapeFromSpawningPoint()
        {
            Vector2 agentPos = new Vector2(agent.transform.position.x, agent.transform.position.z);

            if (spawningPoint != null && (agentPos - spawningPoint).magnitude < 500)
            {
                
                Vector3 targetVector = new Vector3(spawningPoint.x + 2, 0, spawningPoint.y + 2);


                target.position = targetVector;

                //if (agent.remainingDistance > agent.stoppingDistance)
                //    character.Move(agent.desiredVelocity, false, false);
                //else
                //    character.Move(Vector3.zero, false, false);
                //agent.SetDestination(targetVector);
                //character.Move(targetVector, false, false);
                
            }
            //escape = false;
            //Debug.Log("OOO3O");
        }

        void OnCollisionEnter(Collision hit)
        {
            if (hit.gameObject.tag == "green")
            {
                escape = true;
                //EscapeFromSpawningPoint();
            }
        }

    }

}