﻿using UnityEngine;
using System.Collections;

namespace UnityStandardAssets.Characters.ThirdPerson
{
[RequireComponent(typeof (NavMeshAgent))]
[RequireComponent(typeof (ThirdPersonCharacter))]

	public class Cipollini : Default
{
		public NavMeshAgent agent { get; private set; } // the navmesh agent required for the path finding
		public ThirdPersonCharacter character { get; private set; } // the character we are controlling
		public Transform target; // target to aim for

		// Use this for initialization
		private void Start()
		{
			// get the components on the object we need ( should not be null due to require component so no need to check )
			agent = GetComponentInChildren<NavMeshAgent>();
			character = GetComponent<ThirdPersonCharacter>();

			agent.updateRotation = false;
			agent.updatePosition = true;
		}


		// Update is called once per frame
		private void Update()
		{
			GameObject RedBall = findRedBalls();
			if (target != null)
			{
				agent.SetDestination(target.position);
					// use the values to move the character
				character.Move (agent.desiredVelocity, false, false);
			
			}
			else
			{
				SetTarget();
				// We still need to call the character's move function, but we send zeroed input as the move param.
				character.Move(Vector3.zero, false, false);
			}

		}


		public void SetTarget()
		{
			if (GameObject.FindGameObjectsWithTag ("green").Length != 0) {
				double minDist = 1000000;
				GameObject closestBall= new GameObject();
				double d;
				foreach (GameObject ball in GameObject.FindGameObjectsWithTag("green")) {
					d = Vector3.Distance(agent.GetComponent<Transform>().position, ball.GetComponent<Transform>().position);
						if (d< minDist){
							minDist = d; 
							closestBall = ball;
						}
				}
				Vector3 RedBall = findRedBalls().GetComponent<Transform> ().position;
				this.target = closestBall.GetComponent<Transform>();
				if(minDist > Vector3.Distance( RedBall, agent.nextPosition)){
					getAway (RedBall);
				}

			}



		}

		public  GameObject findRedBalls(){
			
			if (GameObject.FindGameObjectsWithTag ("red").Length != 0) {
				double minDist = 1000000;
				GameObject closestBall = new GameObject();
				double d;
				foreach (GameObject ball in GameObject.FindGameObjectsWithTag("red")) {
					d = Vector3.Distance(agent.GetComponent<Transform>().position, ball.GetComponent<Transform>().position);
					if (d< minDist){
						minDist = d; 
						closestBall = ball;
					}
				}
						return closestBall;
			}	
			else
				return null;
		}

		public  void getAway(Vector3 redBall){
			Vector3 newPos = new Vector3 ( - redBall.x, agent.GetComponent<Transform> ().position.y, - redBall.z);
			agent.SetDestination(newPos);
			character.Move (agent.desiredVelocity, false, false);
		}
	}
}
