using System;
using UnityEngine;
using System.Collections;

namespace UnityStandardAssets.Characters.ThirdPerson
{
	[RequireComponent(typeof (NavMeshAgent))]
	[RequireComponent(typeof (ThirdPersonCharacter))]
	public class Conte : Default
	{
		public NavMeshAgent agent { get; private set; }             // the navmesh agent required for the path finding
		public ThirdPersonCharacter character { get; private set; } // the character we are controlling
		public Transform target;// target to aim for

		public static Vector3 myposition;
		GameObject[] verdi,rosse,sfere;

		private class SortByDistance : IComparer
		{

			int System.Collections.IComparer.Compare(object a,object b)
			{
				if (a is GameObject && b is GameObject) {
					GameObject pallaA = ((GameObject)a);
					GameObject pallaB = (GameObject)b;

					if (Vector3.Distance (pallaA.transform.position, myposition) >
						Vector3.Distance (pallaB.transform.position, myposition))
						return 1;
					else if (Vector3.Distance (pallaA.transform.position, myposition) <
						Vector3.Distance (pallaB.transform.position, myposition))
						return -1;
					else
						return 0;

				} else if (a is Collider && b is Collider) {
					Collider pallaA = ((Collider)a);
					Collider pallaB = (Collider)b;

					if (Vector3.Distance (pallaA.transform.position, myposition) >
						Vector3.Distance (pallaB.transform.position, myposition))
						return 1;
					else if (Vector3.Distance (pallaA.transform.position, myposition) <
						Vector3.Distance (pallaB.transform.position, myposition))
						return -1;
					else
						return 0;


				} else
					return 0;
			}

		}


		private void Start()
		{
			// get the components on the object we need ( should not be null due to require component so no need to check )
			agent = GetComponentInChildren<NavMeshAgent>();
			character = GetComponent<ThirdPersonCharacter>();

			agent.updateRotation = false;
			agent.updatePosition = true;
		}


		private void Update()
		{


			myposition = this.transform.position;
			verdi = GameObject.FindGameObjectsWithTag ("green");

			if (verdi.Length > 0) 
			{

				Array.Sort (verdi, (IComparer)new SortByDistance ());
				SetTarget (verdi [0].transform);

			}

			if (target != null)
				agent.SetDestination (target.position);

			if (agent.remainingDistance > agent.stoppingDistance)
				character.Move (agent.desiredVelocity, false, false);
			else
				character.Move (Vector3.zero, false, false);

		}


		public void SetTarget(Transform target)
		{
			this.target = target;
		}
	}
}
