﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Characters.ThirdPerson;

public class AIManager : MonoBehaviour {

	int counter = 0;
	int stop = 0;
	int number = 10;
	int length = 20;

	GameObject aguirreviviani;
	GameObject conte;
	GameObject assantibeccaria;
	GameObject cipollini;
	GameObject mollo;
	GameObject piadaum;
	GameObject laroccabollo;

	int aguirrevivianiScore = 0;
	int conteScore = 0;
	int assantibeccariaScore = 0;
	int cipolliniScore = 0;
	int molloScore = 0;
	int piadaumScore = 0;
	int laroccabolloScore = 0;

	void clear ()
	{
		Destroy(aguirreviviani);
		Destroy( conte);
		Destroy( assantibeccaria);
		Destroy( cipollini);
		Destroy( mollo);
		Destroy( piadaum);
		Destroy( laroccabollo);
	}

	// Use this for initialization
	void Start () 
	{
		GameObject playerObject = GameObject.Find("Player");
		float x = playerObject.transform.position.x;
		float y = playerObject.transform.position.y;
		float z = playerObject.transform.position.z;

		// AguirreViviani
		float dx = Random.value * 10;
		float dz = Random.value * 10;
		aguirreviviani = (GameObject)Instantiate(Resources.Load("AIThirdPersonController"), new Vector3(x+dx,y,z+dz), playerObject.transform.rotation);
		aguirreviviani.name = "AguirreViviani";
		aguirreviviani.AddComponent<AguirreViviani>();
		Debug.Log(aguirreviviani.name + " created");

		// Conte
		dx = Random.value * 10;
		dz = Random.value * 10;
		conte = (GameObject)Instantiate(Resources.Load("AIThirdPersonController"), new Vector3(x+dx,y,z+dz), playerObject.transform.rotation);
		conte.name = "Conte";
		conte.AddComponent<Conte>();
		Debug.Log(conte.name + " created");

		// AssantiBeccaria
		dx = Random.value * 10;
		dz = Random.value * 10;
		assantibeccaria = (GameObject)Instantiate(Resources.Load("AIThirdPersonController"), new Vector3(x+dx,y,z+dz), playerObject.transform.rotation);
		assantibeccaria.name = "AssantiBeccaria";
		assantibeccaria.AddComponent<AssantiBeccaria>();
		Debug.Log(assantibeccaria.name + " created");

		// Cipollini
		dx = Random.value * 10;
		dz = Random.value * 10;
		cipollini = (GameObject)Instantiate(Resources.Load("AIThirdPersonController"), new Vector3(x+dx,y,z+dz), playerObject.transform.rotation);
		cipollini.name = "Cipollini";
		cipollini.AddComponent<Cipollini>();
		Debug.Log(cipollini.name + " created");

		// La RoccaBollo
		dx = Random.value * 10;
		dz = Random.value * 10;
		laroccabollo = (GameObject)Instantiate(Resources.Load("AIThirdPersonController"), new Vector3(x+dx,y,z+dz), playerObject.transform.rotation);
		laroccabollo.name = "LaRoccaBollo";
		laroccabollo.AddComponent<LaRoccaBollo>();
		Debug.Log(laroccabollo.name + " created");

		// Mollo
		dx = Random.value * 10;
		dz = Random.value * 10;
		mollo = (GameObject)Instantiate(Resources.Load("AIThirdPersonController"), new Vector3(x+dx,y,z+dz), playerObject.transform.rotation);
		mollo.name = "Mollo";
		mollo.AddComponent<Mollo>();
		Debug.Log(mollo.name + " created");

		// PiaDaum
		dx = Random.value * 10;
		dz = Random.value * 10;
		piadaum = (GameObject)Instantiate(Resources.Load("AIThirdPersonController"), new Vector3(x+dx,y,z+dz), playerObject.transform.rotation);
		piadaum.name = "PiaDaum";
		piadaum.AddComponent<PiaDaum>();
		Debug.Log(piadaum.name + " created");

		StartCoroutine(stopGame());
	}

	IEnumerator stopGame()
	{
		yield return new WaitForSeconds(length);

		counter++;
		if(stop == 0)
		{
			Debug.Log(" ");
			Debug.Log("Round = " + counter);

			//Debug.Log(aguirreviviani.GetComponent<Default>().name + " = " + aguirreviviani.GetComponent<Default>().score);
			//Debug.Log(conte.GetComponent<Default>().name + " = " + conte.GetComponent<Default>().score);
			//Debug.Log(assantibeccaria.GetComponent<Default>().name + " = " + assantibeccaria.GetComponent<Default>().score);
			//Debug.Log(cipollini.GetComponent<Default>().name + " = " + cipollini.GetComponent<Default>().score);
			//Debug.Log(mollo.GetComponent<Default>().name + " = " + mollo.GetComponent<Default>().score);
			//Debug.Log(piadaum.GetComponent<Default>().name + " = " + piadaum.GetComponent<Default>().score);
			//Debug.Log(laroccabollo.GetComponent<Default>().name + " = " + laroccabollo.GetComponent<Default>().score);

			aguirrevivianiScore 	+= aguirreviviani.GetComponent<Default>().score;
			conteScore 				+= conte.GetComponent<Default>().score;
			assantibeccariaScore 	+= assantibeccaria.GetComponent<Default>().score;
			cipolliniScore 			+= cipollini.GetComponent<Default>().score;
			molloScore 				+= mollo.GetComponent<Default>().score;
			piadaumScore 			+= piadaum.GetComponent<Default>().score;
			laroccabolloScore 		+= laroccabollo.GetComponent<Default>().score;

			aguirreviviani.GetComponent<Default>().score = 0;
			conte.GetComponent<Default>().score = 0;
			assantibeccaria.GetComponent<Default>().score = 0;
			cipollini.GetComponent<Default>().score = 0;
			mollo.GetComponent<Default>().score = 0;
			piadaum.GetComponent<Default>().score = 0;
			laroccabollo.GetComponent<Default>().score = 0;

			if (counter == number) 
			{
				Debug.Log(" ");
				Debug.Log("-------");
				Debug.Log("RESULTS:");

				Debug.Log(aguirreviviani.GetComponent<Default>().name 		+ " = " + (float)aguirrevivianiScore/number);
				Debug.Log(conte.GetComponent<Default>().name 				+ " = " + (float)conteScore/number);
				Debug.Log(assantibeccaria.GetComponent<Default>().name 		+ " = " + (float)assantibeccariaScore/number);
				Debug.Log(cipollini.GetComponent<Default>().name 			+ " = " + (float)cipolliniScore/number);
				Debug.Log(mollo.GetComponent<Default>().name 				+ " = " + (float)molloScore/number);
				Debug.Log(piadaum.GetComponent<Default>().name 				+ " = " + (float)piadaumScore/number);
				Debug.Log(laroccabollo.GetComponent<Default>().name 		+ " = " + (float)laroccabolloScore/number);
				
				stop=1;
			}
		}

		clear();
		Start();
	}

	// Update is called once per frame
	void Update () 
	{
	}
}
