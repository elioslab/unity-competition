using System;
using UnityEngine;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof (NavMeshAgent))]
    [RequireComponent(typeof (ThirdPersonCharacter))]
	public class Mollo : Default
    {
        public NavMeshAgent agent { get; private set; }             // the navmesh agent required for the path finding
        public ThirdPersonCharacter character { get; private set; } // the character we are controlling						 
        public Vector3 targetPosition;// target to aim for


        private void Start()
        {
            // get the components on the object we need ( should not be null due to require component so no need to check )
            agent = GetComponentInChildren<NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();

	        agent.updateRotation = false;
	        agent.updatePosition = true;
        }


        private void Update()
        {
            if (targetPosition != null)
                agent.SetDestination(targetPosition);
            
            GameObject[] greenSpheres = GameObject.FindGameObjectsWithTag("green");
            GameObject[] redSpheres = GameObject.FindGameObjectsWithTag("red");

			//Prendi valore massimo del float
            float min = float.MaxValue;
			//Cerca tra le sfere verdi quella più vicina al giocatore
            if(greenSpheres.Length > 0)
            {
                float x = 0;
                float z = 0;
                foreach (GameObject o in greenSpheres)
                {
                    float distance = Vector3.Distance(o.transform.position, transform.position);
                    x += o.transform.position.x;
                    z += o.transform.position.z;
                    if (distance < min)
                    {
                        min = distance;
                        targetPosition = o.transform.position;
                        targetPosition = new Vector3(targetPosition.x, 0, targetPosition.z);
                    }
                }
				//Calcola inoltre il baricentro
                x /= greenSpheres.Length;
                z /= greenSpheres.Length;
                float space = 40f;
                if (Math.Abs(targetPosition.x - x) > space && Math.Abs(targetPosition.z - z) > space)
                {
                    targetPosition.x = x;
                    targetPosition.z = z;
                }
            }
            // Se ci sono sfere rosse più vicine al giocatore rispetto a quelle verdi allontanati
            foreach (GameObject o in redSpheres)
            {
                float distance = Vector3.Distance(o.transform.position, transform.position);
                float alert = 8f;
                if (distance < min && distance < alert)
                {
                    min = distance;
                    targetPosition = o.transform.position;
                    targetPosition = new Vector3(targetPosition.x * -1, 0, targetPosition.z * -1);
                }
            }
            
        }
    }
}
