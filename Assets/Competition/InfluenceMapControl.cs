using UnityEngine;
using System.Collections;

public class InfluenceMapControl : MonoBehaviour
{
	public Transform bottomLeft;
	public Transform upperRight;
	public float gridSize;
	public int updateFrequency = 3;
	
	InfluenceMap influenceMap;

	public GridDisplay display;

	void CreateMap()
	{
		int width = (int)(Mathf.Abs(upperRight.position.x - bottomLeft.position.x) / gridSize);
		int height = (int)(Mathf.Abs(upperRight.position.z - bottomLeft.position.z) / gridSize);
				
		influenceMap = new InfluenceMap(width, height);
		
		display.SetGridData(influenceMap);
		display.CreateMesh(bottomLeft.position, gridSize);
	}

	public void RegisterPropagator(IPropagator p)
	{
		influenceMap.RegisterPropagator(p);
	}

	public void DeRegisterPropagator(IPropagator p)
	{
		influenceMap.DeRegisterPropagator(p);
	}

	public Vector2I GetGridPosition(Vector3 pos)
	{
		int x = (int)((pos.x - bottomLeft.position.x)/gridSize);
		int y = (int)((pos.z - bottomLeft.position.z)/gridSize);

		return new Vector2I(x, y);
	}

	public void GetMovementLimits(out Vector3 bottomLeft, out Vector3 topRight)
	{
		bottomLeft = this.bottomLeft.position;
		topRight = this.upperRight.position;
	}
	
	void Awake()
	{
		CreateMap();
		InvokeRepeating("PropagationUpdate", 0.001f, 1.0f/updateFrequency);
	}

	void PropagationUpdate()
	{
		influenceMap.Propagate();
	}
}
