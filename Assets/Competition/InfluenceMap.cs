using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public struct Vector2I
{
	public int x;
	public int y;

	public Vector2I(int x, int y)
	{
		this.x = x;
		this.y = y;
	}
}

public class InfluenceMap : GridData
{
	List<IPropagator> propagators = new List<IPropagator>();
	float[,] influences;

	public int Width { get{ return influences.GetLength(0); } }
	public int Height { get{ return influences.GetLength(1); } }
	public float GetValue(int x, int y){ return influences[x, y]; }
	
	public InfluenceMap(int w, int h) {	influences = new float[w, h]; }
		
	public void SetInfluence(int x, int y, float value)
	{
		if (x < Width && y < Height) { influences[x, y] = value; }
	}

	public void updateInfluence(int x, int y, float value)
	{
		if (x < Width && y < Height) { influences[x, y] += value; }
	}

	public void RegisterPropagator(IPropagator p) {	propagators.Add(p);	}
	public void DeRegisterPropagator(IPropagator p) {	propagators.Remove(p);	}

	public void Propagate()
	{
		for (int x = 0; x < influences.GetLength(0); ++x)
		{
			for (int y = 0; y < influences.GetLength(1); ++y) 
			{	
				SetInfluence(x, y, 0); 
			}
		}

		foreach (IPropagator p in propagators)
		{

			int px = p.getGridPosition().x;
			int py = p.getGridPosition().y;
		
			for (int x = 0; x < influences.GetLength(0); ++x)
			{
				for (int y = 0; y < influences.GetLength(1); ++y)
				{
					float value = p.getValue()/(1+ (Mathf.Sqrt((px-x) * (px-x) + (py-y) * (py-y))/2.5f)); 
					updateInfluence(x, y, value); 
				}
			}
		}
	}
}
