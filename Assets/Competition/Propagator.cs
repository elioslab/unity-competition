using UnityEngine;
using System.Collections;

public interface IPropagator
{
	Vector2I getGridPosition();
	float getValue();
}

public class Propagator : MonoBehaviour, IPropagator
{
	public float value;
	public InfluenceMapControl map;

	public float getValue(){
		return value;
	}

	public Vector2I getGridPosition() {
		return map.GetGridPosition(transform.position);
	}

	void Start(){
		map.RegisterPropagator(this);
	}
}
